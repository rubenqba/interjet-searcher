package com.github.rubenqba.flight;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.collection.IsEmptyCollection.empty;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ResponseRepositoryTest {

    @Autowired
    private ResponseRepository responses;

    @Test
    public void getTop1ByAerolineaAndOrigenAndDestinoOrderByCreatedAt() throws Exception {

        Response r = responses.findFirstByAerolineaAndOrigenAndDestinoOrderByCreatedAtDesc("4O", "La Habana", "Monterrey");

        System.out.println(r != null);
    }

    @Test
    public void anteriores() throws Exception {
        Response r = responses.findFirstByAerolineaAndOrigenAndDestinoOrderByCreatedAtDesc("4O", "La Habana",
                "Monterrey");
        List<Response> list = responses.getResultadosAnteriores(r, new PageRequest(0, 10));

        assertThat(list, not(empty()));
    }

    @Test
    @Transactional
    public void anterior() throws Exception {
        Response r = Response.builder()
                .aerolinea("4O")
                .origen("La Habana")
                .fechaSalida(LocalDate.of(2017, 12, 24))
                .tarifaSalida("Tarifai")
                .destino("Monterrey")
                .fechaRegreso(LocalDate.of(2018, 1, 4))
                .tarifaRegreso("Optima")
                .build();

        Optional<Response> before = responses.getResultadoAnterior(r);

        assertThat(before.isPresent(), is(true));
    }
}