ALTER TABLE responses change COLUMN fecha fecha_salida DATE;
ALTER TABLE responses
  ADD fecha_regreso DATE;
ALTER TABLE responses change COLUMN precio precio_salida DOUBLE;
ALTER TABLE responses
  ADD precio_regreso DOUBLE;
ALTER TABLE responses
  ADD precio_total DOUBLE;
ALTER TABLE responses
  ADD tarifa_salida VARCHAR(20);
ALTER TABLE responses
  ADD tarifa_regreso VARCHAR(20);
