CREATE TABLE responses (
  id        BIGINT AUTO_INCREMENT PRIMARY KEY,
  aerolinea VARCHAR(20),
  origen    VARCHAR(20),
  destino   VARCHAR(20),
  fecha     DATE,
  precio    DOUBLE
);