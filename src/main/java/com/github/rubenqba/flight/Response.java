package com.github.rubenqba.flight;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@Data
@Builder
@Entity
@Table(name = "responses")
@NoArgsConstructor
@AllArgsConstructor
public class Response {
    @Id
    @GeneratedValue
    private Long id;

    private String aerolinea;

    private String origen;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate fechaSalida;
    private Double precioSalida;
    private String tarifaSalida;

    private String destino;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate fechaRegreso;
    private Double precioRegreso;
    private String tarifaRegreso;

    private Double precioTotal;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createdAt;
}
