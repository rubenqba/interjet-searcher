package com.github.rubenqba.flight;

import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@Data
@Builder
public class Filter {
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate date;
    private String code;
    private String type;
}
