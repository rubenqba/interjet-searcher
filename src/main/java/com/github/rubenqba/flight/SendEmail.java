package com.github.rubenqba.flight;

import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;

@Service
@Log4j
public class SendEmail {

    private JavaMailSender javaMailSender;
    private InterjetConfigurationProperties config;

    @Autowired
    public SendEmail(JavaMailSender javaMailSender, InterjetConfigurationProperties config) {
        this.javaMailSender = javaMailSender;
        this.config = config;
    }

    public void sendMail(List<String> to, String body, String fileName) {
        if (!to.isEmpty()) {
            log.info("Sending email...");
            MimeMessage message = javaMailSender.createMimeMessage();
            try {
                MimeMessageHelper helper = new MimeMessageHelper(message, true);

                helper.setTo(to.get(0));
                List<String> cc = new ArrayList<>(to.subList(1, to.size()));
                if (!cc.isEmpty()) {
                    helper.setCc(cc.toArray(new String[]{}));
                }
                helper.setFrom(config.getNotifyFrom());
                helper.setSubject("Aviso de precio de Interjet");
                helper.setText(body, true);
                if (StringUtils.isNotBlank(fileName)) {
                    FileSystemResource file = new FileSystemResource(fileName);
                    helper.addAttachment("screenshot.jpg", file);
                }
            } catch (MessagingException e) {
                log.error(e);
            }

            javaMailSender.send(message);

            log.info("Email Sent!");
        }
    }

}