package com.github.rubenqba.flight;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
public interface ResponseRepository extends JpaRepository<Response, Long> {

    @Query("select r " +
            "from Response r " +
            "where r.aerolinea = :#{#actual.aerolinea} " +
            "  and r.origen = :#{#actual.origen} " +
            "  and r.tarifaSalida = :#{#actual.tarifaSalida} " +
            "  and r.fechaSalida = :#{#actual.fechaSalida} " +
            "  and r.destino = :#{#actual.destino} " +
            "  and r.tarifaRegreso = :#{#actual.tarifaRegreso} " +
            "  and r.fechaRegreso = :#{#actual.fechaRegreso} " +
            "order by r.createdAt desc ")
    List<Response> getResultadosAnteriores(@Param("actual") Response before, Pageable page);

    default Optional<Response> getResultadoAnterior(Response before) {
        List<Response> list = getResultadosAnteriores(before, new PageRequest(0, 1));
        return Optional.ofNullable(list.isEmpty() ? null : list.get(0));
    }

    Response findFirstByAerolineaAndOrigenAndDestinoOrderByCreatedAtDesc(String aerolinea,
                                                                         String origen,
                                                                         String destino);
}
