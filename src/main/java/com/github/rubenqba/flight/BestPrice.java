package com.github.rubenqba.flight;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@Data
@AllArgsConstructor
public class BestPrice {
    private LocalDate fecha;
    private Double precio;
}
