package com.github.rubenqba.flight;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@Data
@Builder
public class Request {
    private Filter from;
    private Filter to;
    private Double maxPrice;
    private Double buyPrice;
    private List<String> notifyTo;
}
