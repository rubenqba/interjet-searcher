package com.github.rubenqba.flight;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.annotation.PostConstruct;
import java.util.Locale;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@ConfigurationProperties(prefix = "interjet")
@Configuration
@Getter
@Setter
public class InterjetConfigurationProperties {
    private String language;
    private String country;

    private String notifyFrom;
    private String driver;
    private String googleApplicationName;
    private String googleDocId;

    @PostConstruct
    public void configureLocale() {
        Locale.setDefault(Locale.forLanguageTag(language + "-" + country));
        LocaleContextHolder.setDefaultLocale(Locale.getDefault());
    }
}
