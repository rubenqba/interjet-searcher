package com.github.rubenqba.flight;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.tools.generic.DateTool;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@Component
@Log4j
public class SearchFlights /*implements CommandLineRunner*/ {

    private ApplicationContext ctx;

    private InterjetConfigurationProperties config;

    private SendEmail notification;

    private ScheduledExecutorService exec;

    private ResponseRepository response;

    private Sheets sheets;

    @Autowired
    public SearchFlights(ApplicationContext ctx, InterjetConfigurationProperties config, SendEmail notification,
                         ScheduledExecutorService exec, Sheets sheets, ResponseRepository response) {
        this.ctx = ctx;
        this.config = config;
        this.notification = notification;
        this.exec = exec;
        this.sheets = sheets;
        this.response = response;
    }

//    @Override
//    public void run(String... strings) throws Exception {
//        exec.scheduleAtFixedRate(
//                () -> getRequests().forEach(SearchFlights.this::searchFlight), 0, 1, TimeUnit.HOURS);
//
//    }

    @Scheduled(initialDelay = 10000, fixedRate = 60 * 60 * 1000)
    public void execute() {
        getRequests().stream()
                .flatMap(r -> searchFlight(r).stream())
                .forEach(response::save);
    }

    private List<Request> getRequests() {
        log.debug("recuperando lista de chequeo...");
        try {
            ValueRange response = sheets.spreadsheets().values()
                    .get(config.getGoogleDocId(), "flights!A2:H")
                    .execute();

            List<List<Object>> values = response.getValues();
            if (values == null || values.size() == 0) {
                log.info("No data found.");
            } else {
                log.debug("hay elementos que buscar");
                return values.stream()
                        .map(row -> Request.builder()
                                .from(Filter.builder()
                                        .code((String) row.get(0))
                                        .date(LocalDate.parse((String) row.get(2)))
                                        .type((String) row.get(3))
                                        .build())
                                .to(Filter.builder()
                                        .code((String) row.get(1))
                                        .date(LocalDate.parse((String) row.get(4)))
                                        .type((String) row.get(5))
                                        .build())
                                .maxPrice(Double.parseDouble((String) row.get(6)))
                                .notifyTo(Arrays.asList(((String) row.get(7)).split(",")))
                                .build())
                        .collect(Collectors.toList());
            }

        } catch (IOException e) {
            log.error(e);
        }
        return new ArrayList<>();
    }

    private List<Response> searchFlight(Request req) {
        ArrayList<Response> list = new ArrayList<>();
        Response.ResponseBuilder builder = Response.builder()
                .aerolinea("4O")
                .origen(req.getFrom().getCode())
                .fechaSalida(req.getFrom().getDate())
                .tarifaSalida(req.getFrom().getType())
                .destino(req.getTo().getCode())
                .fechaRegreso(req.getTo().getDate())
                .tarifaRegreso(req.getTo().getType());

        RemoteWebDriver driver = ctx.getBean(RemoteWebDriver.class);

        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);

            //Launch the Online Store Website
            driver.get("https://www.interjet.com.mx/ScheduleSelect.aspx");

            loginInterjet(driver, req);
            Thread.sleep(1000);

            driver.get("https://www.interjet.com.mx/ScheduleSelect.aspx");
            wait.until(ExpectedConditions.elementToBeClickable(By.id("ClickSearchFlight")));
            driver.findElement(By.id("ClickSearchFlight")).click();

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='OriginStation']")));
            driver.findElement(By.xpath("//input[@id='OriginStation']")).clear();
            driver.findElement(By.xpath("//input[@id='OriginStation']")).sendKeys(req.getFrom().getCode());

            Thread.sleep(1000);
            driver.findElement(By.xpath("//input[@id='OriginStation']")).sendKeys(Keys.DOWN);
            driver.findElement(By.xpath("//input[@id='OriginStation']")).sendKeys(Keys.ENTER);

            driver.findElement(By.xpath("//input[@id='DestinationStation']")).clear();
            driver.findElement(By.xpath("//input[@id='DestinationStation']")).sendKeys(req.getTo().getCode());

            Thread.sleep(1000);
            driver.findElement(By.xpath("//input[@id='DestinationStation']")).sendKeys(Keys.DOWN);
            driver.findElement(By.xpath("//input[@id='DestinationStation']")).sendKeys(Keys.ENTER);

            // seleccionar día inicio.
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='marketDate_1']/img")));
            driver.findElement(By.xpath("//div[@id='marketDate_1']/img")).click();
            selectDay(driver, req.getFrom().getDate());

            // seleccionar dia regreso
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='marketDate_2']/img")));
            driver.findElement(By.xpath("//div[@id='marketDate_2']/img")).click();
            selectDay(driver, req.getTo().getDate());

            driver.findElement(By.id("AvailabilitySearchInputScheduleSelectAView_ButtonSubmit")).click();

            Double bestPrice = getBestPrice(driver, String.format("0_%%d_%s_div", req.getFrom().getType()));
            System.out.println(new BestPrice(req.getFrom().getDate(), bestPrice));

            builder.precioSalida(bestPrice);

            bestPrice = getBestPrice(driver, String.format("1_%%d_%s_div", req.getTo().getType()));
            System.out.println(new BestPrice(req.getTo().getDate(), bestPrice));
            builder.precioRegreso(bestPrice);

            Thread.sleep(1000); // esperar un momento a que se actualize el total
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.total > span")));
            Double precioFinal = Double.valueOf(driver.findElement(By.cssSelector("div.total > span")).getText()
                    .split("\\s+")[0]
                    .replace("$", "")
                    .replaceAll(",", ""));

            builder.precioTotal(precioFinal);

            Response result = builder.build();

            Response last = response.getResultadoAnterior(result)
                    .orElse(null);

            list.add(result);
            if (precioFinal < req.getMaxPrice() || (last != null && last.getPrecioTotal() > precioFinal)) {
                screenShot(driver, "interjet");
                notification.sendMail(req.getNotifyTo(),
                        buildBody(req, precioFinal, "interjet.jpg"),
                        "interjet.jpg");
            }

        } catch (InterruptedException e) {
            log.error(e);
        } finally {
            savePage(driver, "interjet");
            screenShot(driver, "interjet");
            driver.close();
        }
        return list;
    }

    private String buildBody(Request req, Double precioFinal, String image) {
        try {
            VelocityContext context = new VelocityContext();
            context.put("date", new DateTool());
            context.put("req", req);
            context.put("dateFrom", Date.from(req.getFrom().getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
            context.put("dateTo", Date.from(req.getTo().getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
            context.put("price", precioFinal);
//            context.put("image", getScreenshotAsBase64(image));
            InputStream in = SearchFlights.class.getResourceAsStream("/notificacion.html");
            StringBuilder out = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    out.append(line);
                }
            }
            return getMessage(out.toString(), context);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    private String getScreenshotAsBase64(String image) throws IOException {
        return Base64Utils.encodeToString(FileUtils.readFileToByteArray(new File(image)));
    }

    private String getMessage(String template, VelocityContext contexto) {
        StringWriter stWriter = new StringWriter();
        Reader templateReader = new BufferedReader(
                new InputStreamReader(
                        new ByteArrayInputStream(template.getBytes(Charset.forName("UTF-8"))), Charset.forName("UTF-8")));
        Velocity.setProperty("runtime.references.strict", "true");
        Velocity.evaluate(contexto, stWriter, getClass().getSimpleName(), templateReader);

        return stWriter.toString();
    }

    private void loginInterjet(RemoteWebDriver driver, Request req) {
        WebDriverWait wait = new WebDriverWait(driver, 20);

        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.id("CreditAccountInformation")));
            String login = driver.findElement(By.id("CreditAccountInformation")).getText();
        } catch (TimeoutException | NoSuchElementException e) {
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".login-register a")));
            driver.findElement(By.xpath("//a[text()='Ingresa']")).click();

            driver.findElement(By.id("TextBoxUserID")).sendKeys("ruben1981@gmail.com");
            driver.findElement(By.id("TextBoxUserID")).sendKeys(Keys.TAB);
            driver.findElement(By.id("PasswordFieldPassword")).sendKeys("ciberbsbneo");
            driver.findElement(By.id("PasswordFieldPassword")).sendKeys(Keys.TAB);
            driver.findElement(By.id("LoginButton")).click();
        }
    }

    private void selectDay(RemoteWebDriver driver, LocalDate date) {


        /**
         *  1 - verificar que en la izquierda no esté el mes que se busca
         *  2- si true, seleccionar dia
         *  3- sino verificar la derecha
         *  4- si true, seleccionar dia
         *  5- sino click en next, y verificar si la derecha es el mes
         *  6- si true, seleccionar dia
         *  7- sino repetir desde 5
         */

        if (verifyMonthIn(driver, ".ui-datepicker-group-first", date)) { // izquierda
            selectDayOfMonth(driver, true, date);
        } else if (verifyMonthIn(driver, ".ui-datepicker-group-last", date)) { // derecha
            selectDayOfMonth(driver, false, date);
        } else {
            // si cae aqui, siempre estará a la derecha
            do {
                // mientras no aparezca a la derecha
                nextMonth(driver);
            } while (!verifyMonthIn(driver, ".ui-datepicker-group-last", date));
            selectDayOfMonth(driver, false, date);
        }
    }

    private void nextMonth(RemoteWebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 5);

        wait.until(ExpectedConditions.elementToBeClickable(By.className("ui-datepicker-next")));
        driver.findElement(By.className("ui-datepicker-next")).click();
    }

    private void selectDayOfMonth(RemoteWebDriver driver, boolean b, LocalDate date) {
        int pos = 2; // derecha
        if (b) {
            // izquierda
            pos = 1;
        }
        driver.findElement(By.xpath(String.format("//div[@id='ui-datepicker-div']/div[%d]//a[text()='%d']",
                pos, date.getDayOfMonth()))).click();
    }

    private boolean verifyMonthIn(RemoteWebDriver driver, String className, LocalDate date) {
        WebDriverWait wait = new WebDriverWait(driver, 5);

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(className + " .ui-datepicker-year")));
        WebElement year = driver.findElement(By.cssSelector(className + " .ui-datepicker-year"));

        String str = String.valueOf(date.getYear());
        if (!str.equalsIgnoreCase(year.getText())) {
            return false;
        }

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(className + " .ui-datepicker-month")));
        WebElement month = driver.findElement(By.cssSelector(className + " .ui-datepicker-month"));
        str = String.valueOf(date.getMonth()
                .getDisplayName(TextStyle.FULL, LocaleContextHolder.getLocale()));
        return str.equalsIgnoreCase(month.getText());
    }

    private Double getBestPrice(RemoteWebDriver driver, String idPattern) {
        WebDriverWait wait = new WebDriverWait(driver, 10);

        boolean salir = false;
        int tarifaiIndex = 0;
        Double minPrice = Double.MAX_VALUE;
        int index = Integer.MAX_VALUE;

        wait.until(ExpectedConditions.elementToBeClickable(By.id(String.format(idPattern, tarifaiIndex))));
        do {
            try {
                WebElement tarifaI = driver.findElement(By.id(String.format(idPattern, tarifaiIndex)));
                Double precio = Double.parseDouble(tarifaI.getText()
                        .split("\\s+")[0]
                        .replace("$", "")
                        .replaceAll(",", ""));
                if (minPrice > precio) {
                    index = tarifaiIndex;
                    minPrice = new Double(precio);
                }
                tarifaiIndex++;
            } catch (Exception e) {
                salir = true;
            }
        } while (!salir);

        if (index < Integer.MAX_VALUE && minPrice != Double.MAX_VALUE) {
            driver.findElement(By.id(String.format(idPattern, index))).click();
        }

        return minPrice;
    }

    private void screenShot(RemoteWebDriver driver, String fn) {
        byte[] jpg = driver.getScreenshotAs(OutputType.BYTES);

        File f = new File(fn + ".jpg");
        try (FileOutputStream fos = new FileOutputStream(f)) {
            fos.write(jpg);
        } catch (IOException e) {
            log.error(e);
        }
    }

    private void savePage(RemoteWebDriver driver, String fn) {
        String html = driver.getPageSource();
        String xml = prettyXML(html);
        try {
            Files.write(Paths.get(fn + ".html"), xml.getBytes());
        } catch (IOException e) {
            log.error(e);
        }
    }

    private String prettyXML(String xml) {
        try {
            Document doc = Jsoup.parse(xml);
            doc.outputSettings().indentAmount(4);
            return doc.toString();
        } catch (Exception e) {
//            log.error("Error formateando XML",e);
            return xml;
        }
    }
}
