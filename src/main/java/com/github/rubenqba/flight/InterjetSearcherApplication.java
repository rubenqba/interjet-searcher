package com.github.rubenqba.flight;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableScheduling
@EnableJpaAuditing
public class InterjetSearcherApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterjetSearcherApplication.class, args);
    }

    @Bean(destroyMethod = "quit")
    @Scope("prototype")
    public RemoteWebDriver createDriver(Environment env) {
        String driver = env.getRequiredProperty("interjet.driver");

        RemoteWebDriver webDriver;
        if ("chrome".equalsIgnoreCase(driver)) {
            webDriver = createChromeDriver(env);
        } else if ("firefox".equalsIgnoreCase(driver)) {
            webDriver = createFirefoxDriver(env);
        } else if ("phantomjs".equalsIgnoreCase(driver)) {
            webDriver = createPhantomJs(env);
        } else if ("remote".equalsIgnoreCase(driver)) {
            webDriver = createRemoteHub(env);
        } else {
            throw new IllegalArgumentException("driver incorrecto ['chrome', 'firefox', 'phantomjs']");
        }

        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        webDriver.manage().window().setSize(new Dimension(1024, 768));

        return webDriver;
    }

    @Bean
    @ConfigurationPropertiesBinding
    public Converter<String, LocalDate> dateConverter() {
        return new Converter<String, LocalDate>() {
            @Override
            public LocalDate convert(String s) {
                return StringUtils.isNotBlank(s) ? LocalDate.parse(s, DateTimeFormatter.ISO_DATE) : null;
            }
        };
    }

    private RemoteWebDriver createChromeDriver(Environment env) {
        ChromeOptions options = new ChromeOptions();
        if (StringUtils.isNotBlank(env.getProperty("webdriver.path.chrome"))) {
            System.setProperty("webdriver.chrome.driver", env.getProperty("webdriver.path.chrome"));
        }
        return new ChromeDriver(options);
    }


    private RemoteWebDriver createPhantomJs(Environment env) {
        return new PhantomJSDriver();
    }

    private RemoteWebDriver createFirefoxDriver(Environment env) {
        FirefoxOptions options = new FirefoxOptions();
        FirefoxBinary binary = new FirefoxBinary(new File("C:\\Program Files\\Mozilla Firefox\\firefox.exe"));
        options.setBinary(binary);
        if (StringUtils.isNotBlank(env.getProperty("webdriver.path.gecko"))) {
            System.setProperty("webdriver.gecko.driver", env.getProperty("webdriver.path.gecko"));
        }
        return new FirefoxDriver(options);
    }

    private RemoteWebDriver createRemoteHub(Environment env) {
        try {
            return new RemoteWebDriver(new URL("http://pwd10-0-19-3-32768.host1.labs.play-with-docker.com/wd/hub"), DesiredCapabilities.chrome());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    public ScheduledExecutorService createExceutor() {
        return Executors.newSingleThreadScheduledExecutor();
    }

}
